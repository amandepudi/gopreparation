Go Language Prep

----------------------------------------------
Add Course Resources Here.

 Why Go ?
 1. Efficient Compilation
 2. Efficient Execution
 3. Ease of Programming


What Go is Good For ?
1. Web Services at Scale
2. Networking
3. Concurrency / Parallelism
4. Automation, Command-line Tools

Course Resources Link : http://bit.ly/2Nih2a9

Go Commands:
1. go help
2. go fmt main.go  
3. go run main.go
4. go build