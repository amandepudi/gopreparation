package main

import "fmt"

// main() : entry point to the program
// fileName can be anything

func main() { //{  this should be right after the funtion declaration
	fmt.Println("File Name can be Anything")
	something()
}
func something() {
	fmt.Println("I'm in something fn")
}
